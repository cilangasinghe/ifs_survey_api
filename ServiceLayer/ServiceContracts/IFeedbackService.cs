﻿using ServiceLayer.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.ServiceContracts
{
   public interface IFeedbackService
    {
       FeedbackDto AddFeedback(FeedbackDto FeedbackToBeAdded);

         List<FeedbackDto> GetFeedbacks();
    }
}
