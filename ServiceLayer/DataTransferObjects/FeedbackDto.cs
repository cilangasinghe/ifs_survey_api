﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.DataTransferObjects
{
    public class FeedbackDto
    {


        public String FeedbackCaseId { get; set; }

        public String FeedbackTaskId { get; set; }

        public String FeedbackUserId { get; set; }

        public String FeedbackOwnerId { get; set; }

        public DateTime? FeedbackRecDate { get; set; }

        public String FeedBackModifiedBy { get; set; }

        public DateTime? FeedbackModifiedDate { get; set; }

        public String FeedbackComment { get; set; }

        public String FeedbackAnalyticalSkills { get; set; }

        public String FeedbackQualityTime { get; set; }

        public String FeedbackCommunication { get; set; }


    }
}
