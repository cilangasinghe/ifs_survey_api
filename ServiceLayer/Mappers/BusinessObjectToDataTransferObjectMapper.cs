﻿using BusinessLogicLayer.BusinessObjects;
using ServiceLayer.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Mappers
{
    public class BusinessObjectToDataTransferObjectMapper
    {
        public static FeedbackDto MapFeedbackBusinessObjectToDataTransferObject(FeedbackBusinessObject businessObjectToBeMapped)
        {
            FeedbackDto feedbackDataTransferObject = new FeedbackDto() 
            {
                FeedbackCaseId = businessObjectToBeMapped.CaseId,
                FeedbackTaskId = businessObjectToBeMapped.TaskId,
                FeedbackUserId = businessObjectToBeMapped.UserId,
                FeedbackOwnerId = businessObjectToBeMapped.OwnerId,
                FeedBackModifiedBy = businessObjectToBeMapped.ModifiedBy,
                FeedbackComment = businessObjectToBeMapped.Comment,
                FeedbackAnalyticalSkills = businessObjectToBeMapped.AnalyticalSkills,
                FeedbackQualityTime = businessObjectToBeMapped.QualityTime,
                FeedbackCommunication = businessObjectToBeMapped.Communication,
                FeedbackRecDate = businessObjectToBeMapped.RecDate,
                FeedbackModifiedDate = businessObjectToBeMapped.ModifiedDate,
            };
            return feedbackDataTransferObject;
        }
    }
}
