﻿using BusinessLogicLayer.BusinessObjects;
using ServiceLayer.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Mappers
{
    public class DataTransferObjectToBusinessObjectMapper
    {
        public static FeedbackBusinessObject MapFeedbackDataTransferObjectToBusinessObject(FeedbackDto dataTransferObjectToBeMapped)
        {
            FeedbackBusinessObject businessObject = new FeedbackBusinessObject()
            { 
                CaseId = dataTransferObjectToBeMapped.FeedbackCaseId,
                TaskId = dataTransferObjectToBeMapped.FeedbackTaskId,
                UserId = dataTransferObjectToBeMapped.FeedbackUserId,
                OwnerId = dataTransferObjectToBeMapped.FeedbackOwnerId,
                ModifiedBy = dataTransferObjectToBeMapped.FeedBackModifiedBy,
                Comment = dataTransferObjectToBeMapped.FeedbackComment,
                AnalyticalSkills = dataTransferObjectToBeMapped.FeedbackAnalyticalSkills,
                QualityTime = dataTransferObjectToBeMapped.FeedbackQualityTime,
                Communication = dataTransferObjectToBeMapped.FeedbackCommunication,
                RecDate = dataTransferObjectToBeMapped.FeedbackRecDate,
                ModifiedDate = dataTransferObjectToBeMapped.FeedbackModifiedDate

            };
            return businessObject;
        } 
    }
}
