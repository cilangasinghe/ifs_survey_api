﻿using BusinessLogicLayer.BusinessObjects;
using ServiceLayer.DataTransferObjects;
using ServiceLayer.Mappers;
using ServiceLayer.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.ServiceImplementations
{
    public class FeedbackService:IFeedbackService
    {
        FeedbackDto IFeedbackService.AddFeedback(DataTransferObjects.FeedbackDto insertedFeedback)
        {
            FeedbackBusinessObject feedbackToBeAdded = DataTransferObjectToBusinessObjectMapper.MapFeedbackDataTransferObjectToBusinessObject(insertedFeedback);
            FeedbackBusinessObject result = feedbackToBeAdded.SaveFeedback();
            return BusinessObjectToDataTransferObjectMapper.MapFeedbackBusinessObjectToDataTransferObject(result);
        }

        List<DataTransferObjects.FeedbackDto> IFeedbackService.GetFeedbacks()
        {
            FeedbackBusinessObject feedbackbusinessObject = new FeedbackBusinessObject();
            List<FeedbackBusinessObject> result = feedbackbusinessObject.getFeedbacks();
            List<FeedbackDto> feedbackList = new List<FeedbackDto>();

            foreach(FeedbackBusinessObject currentFeedback in result){
                feedbackList.Add(BusinessObjectToDataTransferObjectMapper.MapFeedbackBusinessObjectToDataTransferObject(currentFeedback));
            }

            return feedbackList;
        }
    }
}
