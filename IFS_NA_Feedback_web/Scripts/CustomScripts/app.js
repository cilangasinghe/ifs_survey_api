﻿(function () {
    'use strict';
    //var surveyAppControllers = angular.module('surveyApp.controllers', []);
    var surverApp = angular.module('surveyApp', ['ui.router']);
   

    surverApp.config(function ($stateProvider,$urlRouterProvider) {
        $urlRouterProvider.otherwise('/home/home');
        $stateProvider.state('Home', {
            url: '/home/home',
            templateUrl: '/Home/Home',
            controller:'employeeHomeController'
        });

    });



    surverApp.controller('employeeHomeController', ['$scope', function ($scope) {
        $scope.message = "Employee home controller";
        function loadData() {
            window.alert("Hello!");
        }
        loadData();
    }]);

}());