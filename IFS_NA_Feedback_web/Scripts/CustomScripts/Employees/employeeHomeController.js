﻿(function () {
    'use strict';
    var surveyAppControllers = angular.module('surveyApp.controllers',[]);

    surveyAppControllers.controller('employeeHomeController', ['$scope', function ($scope) {
        $scope.message = "Employee home controller";
        function loadData() {
            window.alert("Hello!");
        }
        loadData();
    }]);

}());