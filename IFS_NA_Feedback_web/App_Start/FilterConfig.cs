﻿using System.Web;
using System.Web.Mvc;

namespace IFS_NA_Feedback_web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
