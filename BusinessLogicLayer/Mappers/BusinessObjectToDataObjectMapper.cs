﻿using BusinessLogicLayer.BusinessObjects;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Mappers
{
   public class BusinessObjectToDataObjectMapper
    {
       public static SurveyData MapFeedbackBusinessObjectToDataObject(FeedbackBusinessObject businessObjectToBeMapped)
       {
           SurveyData feedbackDataObject = new SurveyData()
           {

               FeedbackCaseId = businessObjectToBeMapped.CaseId,
                FeedbackTaskId = businessObjectToBeMapped.TaskId,
                FeedbackUserId = businessObjectToBeMapped.UserId,
                FeedbackOwnerId = businessObjectToBeMapped.OwnerId,
                FeedBackModifiedBy = businessObjectToBeMapped.ModifiedBy,
                FeedbackModifiedDate=businessObjectToBeMapped.ModifiedDate,
                FeedbackRecDate=businessObjectToBeMapped.RecDate,
                FeedbackCommunication=businessObjectToBeMapped.Communication,
                FeedbackComment = businessObjectToBeMapped.Comment,
                FeedbackAnalyticalSkills = businessObjectToBeMapped.AnalyticalSkills,
                FeedbackQualityTime = businessObjectToBeMapped.QualityTime,
           };
           return feedbackDataObject;
       }
    }
}
