﻿using BusinessLogicLayer.BusinessObjects;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Mappers
{
    class DataObjectToBusinessObjectMapper
    {
        public static FeedbackBusinessObject MapFeedbackDataObjectToFeedbackBusinessObject(SurveyData feedbackDataObject)
        {
            FeedbackBusinessObject mappedBusinessObject=new FeedbackBusinessObject()
            {
                CaseId = feedbackDataObject.FeedbackCaseId,
                TaskId = feedbackDataObject.FeedbackTaskId,
                UserId = feedbackDataObject.FeedbackUserId,
                OwnerId = feedbackDataObject.FeedbackOwnerId,
                ModifiedBy = feedbackDataObject.FeedBackModifiedBy,
                Comment = feedbackDataObject.FeedbackComment,
                AnalyticalSkills = feedbackDataObject.FeedbackAnalyticalSkills,
                QualityTime = feedbackDataObject.FeedbackQualityTime,
                Communication = feedbackDataObject.FeedbackCommunication,
                RecDate = feedbackDataObject.FeedbackRecDate,
                ModifiedDate = feedbackDataObject.FeedbackModifiedDate
            };
            return mappedBusinessObject;
        }
    }
}
