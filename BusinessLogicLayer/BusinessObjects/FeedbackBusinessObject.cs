﻿using BusinessLogicLayer.Mappers;
using DataAccessLayer;
using DataAccessLayer.DataAccessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.BusinessObjects
{
    public class FeedbackBusinessObject
    {
        public String CaseId { get; set; }
        public String TaskId { get; set; }
        public String UserId { get; set; }
        public String OwnerId { get; set; }
        public String ModifiedBy { get; set; }
        public String Comment { get; set; }
        public String AnalyticalSkills { get; set; }
        public String QualityTime { get; set; }
        public String Communication { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? RecDate { get; set; }

        public FeedbackBusinessObject SaveFeedback()
        {
            FeedbackDao feedbackDataAccessObject = new FeedbackDao();
            SurveyData result = feedbackDataAccessObject.SaveFeedback(BusinessObjectToDataObjectMapper.MapFeedbackBusinessObjectToDataObject(this));
            return DataObjectToBusinessObjectMapper.MapFeedbackDataObjectToFeedbackBusinessObject(result);
        }

        public List<FeedbackBusinessObject> getFeedbacks()
        {
            FeedbackDao emplyeeDataAccessObject = new FeedbackDao();
            List<SurveyData> result = emplyeeDataAccessObject.GetFeedbacks();
            List<FeedbackBusinessObject> feedbackList = new List<FeedbackBusinessObject>();

            foreach (SurveyData currentFeedback in result)
            {
                feedbackList.Add(DataObjectToBusinessObjectMapper.MapFeedbackDataObjectToFeedbackBusinessObject(currentFeedback));
            }

            return feedbackList;

        }

       
    }
}
