﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataAccessObjects
{
    public class FeedbackDao
    {
        SurveryAppDbEntities dataAccessLayer;

        public FeedbackDao()
        {
            dataAccessLayer = new SurveryAppDbEntities();
        }
        public SurveyData SaveFeedback(SurveyData feedbackToBeSaved)
        {
            try
            {
                dataAccessLayer.SurveyDatas.Add(feedbackToBeSaved);
                dataAccessLayer.SaveChanges();
                return dataAccessLayer.SurveyDatas.First<SurveyData>();
            }catch(Exception e){
               //Todo
            }
            return dataAccessLayer.SurveyDatas.First<SurveyData>();
        }

        public List<SurveyData> GetFeedbacks()
        {
            List<SurveyData> feedbacksInDb = dataAccessLayer.SurveyDatas.Select(r1 => r1).ToList<SurveyData>();
            return feedbacksInDb;
        }


    }
}
