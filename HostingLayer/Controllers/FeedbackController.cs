﻿using ServiceLayer.DataTransferObjects;
using ServiceLayer.ServiceContracts;
using ServiceLayer.ServiceImplementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HostingLayer.Controllers
{
    public class FeedbackController : ApiController
    {
        IFeedbackService feedbackService;

        public FeedbackController()
        {
            feedbackService = new FeedbackService();
        }
        // POST: api/Feedback
        public FeedbackDto Post(FeedbackDto submittedFeedback)
        {
            FeedbackDto result = feedbackService.AddFeedback(submittedFeedback);
            return result; 
 
        }

        public List<FeedbackDto> Get()
        {
            List<FeedbackDto> feedbackList = feedbackService.GetFeedbacks();
            return feedbackList;
        }

       
        
    }
}
